console.log('======= Ascending Ten =======' + '\n')

function AscendingTen(num) {
    if (num == null || num == "")
        return -1;
    var data = "";
    for (var i = num; i < num + 10; i++) {
        data += (i + " ");
    }
    return data;
}

console.log(AscendingTen(1))
console.log(AscendingTen(101))

console.log('\n' + '======= Descending Ten =======' + '\n')
function DescendingTen(num) {
    if(num === null || num == '')
        return -1;
        var data = '';
        for (i = num; i > num - 10; i--){
            data +=  (i + " ")
        }
        return data
}

console.log(DescendingTen(10))
console.log(DescendingTen(20))

console.log('\n' + '=======  function ConditionalAscDesc =======' + '\n')

function ConditionalAscDesc(reference, check) {
    if (reference == null || reference == '' && check == null || check == '')
        return -1;
        var data = '';
        if(check % 2 === 0) 
            data = DescendingTen(reference)
            else data = AscendingTen(reference);
        return data;
}

console.log(ConditionalAscDesc(1, 1))
console.log(ConditionalAscDesc(100, 4))

console.log('\n' + '=======  function ularTangga =======' + '\n')

function ularTangga(height = 10) { // nilai genap untuk tinggi
    height *= 10;
    while (height >= 1) {
        if (height % 2 == 0) { // turun
            console.log(DescendingTen(height));
            height -= 19;
        } else { // naik
            console.log(AscendingTen(height));
            height -= 1;
        }
    }
    return "";
}

console.log(ularTangga())
