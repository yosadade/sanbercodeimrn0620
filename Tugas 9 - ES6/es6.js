// tugas 1
console.log('======= tugas 1 =======')
const golden = () => {
    console.log('this s golden !!')
}

golden()

// tugas 2
console.log('======= tugas 2 =======')
const newFunction = (firstName, lastName) => {
    return {
        firstName, 
        lastName, 
        fullName: () => {
            console.log(firstName +  " " + lastName)
            return
        }
    }
}

newFunction("William", "Imoh").fullName() 

// tugas 3
console.log('======= tugas 3 =======')
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const { firstName, lastName, destination, occupation, spell} = newObject
console.log(`${firstName}, ${lastName}, ${destination}, ${occupation}`)

// tugas 4
console.log('======= tugas 4 =======')
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combine = [...west, ...east]
console.log(combine)

// tugas 5
console.log('======= tugas 5 =======')
const planet = "earth"
const view = "glass"

const before = `Lorem ${view} dolar sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)