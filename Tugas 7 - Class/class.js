// Soal 1 
class Animal {
    constructor(name) {
        this.name = name
        this.legs = 4
        this.cold_blooded = false
    }
}

var sheep = new Animal("shaun")
console.log('======= SOAL 1 =======')
console.log(sheep.name)
console.log(sheep.legs)
console.log(sheep.cold_blooded)

class Ape extends Animal {
    constructor(name) {
        super(name);
        this.legs = 2
    }
    yell() {
        console.log("Auooo")
    }
}
var sungokong = new Ape ("Kera Sakti")
sungokong.yell()

class Frog extends Animal {
    constructor(name) {
        super(name)
    }
    jump(){
        console.log("hop hop")
    }
}

var kodok = new Frog ("buduk")
kodok.jump()

// Soal 2
console.log('======= Soal 2 =======')
class Clock {
    constructor({ template }) {
        this.tempate = template
        var timer;
    }
    render(){
        var date = new Date();
  
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        // pengganti var output
        this.tempate = hours + ":" + mins + ":" + secs + ":"
        console.log(this.tempate)
    }

    stop() {
        clearInterval(timer)
    }
    start() {
        this.render();
        this.timer = setInterval(this.render, 1000)
    }
}

var clock = new Clock({
    template: 'h:m:s'
});
clock.start();; 
