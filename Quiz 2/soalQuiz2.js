// Soal 1
class Score {
    constructor(subject,points,email){
        this.subject=subject;
        this.points=points;
        this.email=email;
    }
    average(){
        if(Array.isArray(this.points)) {
        var sum=0;
        for(var i=0; i<this.points.length; i++ ){
            sum += parseInt( this.points[i]);
        }
        return sum/this.points.length;
        }
        else  return this.points;
    }
}
  
  var score = new Score("Sosiologi",[8,9,9.5],'masyosad@gmail.com');
  //var score = new Score("Sosiologi",[9],'masyosad@gmail.com');
  console.log(score.average());
  

// Soal 2
const data = [
    ["email", "quiz-1", "quiz-2", "quiz-3"],
    ["abduh@mail.com", 78, 89, 90],
    ["khairun@mail.com", 95, 85, 88],
    ["bondra@mail.com", 70, 75, 78],
    ["regi@mail.com", 91, 89, 93]
  ]
  //console.log(data.length)
const viewScores = (data, subject) => {
    let arrJudul = data[0];
    let arr = data.slice(1,data.length);
    let arrBalikan = [];
    for (let i=0; i < arr.length; i++){
        let newObj = {}
        newObj.email = arr[i][0];
        newObj.subject = subject;
        newObj.points = arr[i][arrJudul.indexOf(subject)];
        arrBalikan.push(newObj);
    }
    console.log(arrBalikan);
}

viewScores(data, "quiz-1")
viewScores(data, "quiz-2")
viewScores(data, "quiz-3")
  

// Soal 3
const recapScores = (data) => {
    let arr = data.slice(1,data.length);
    for (let i=0; i < arr.length; i++){
        let average = (arr[i][1]+arr[i][2]+arr[i][3])/3;
      let predikat = '';
      if(average > 90){
          predikat = 'honour';
      } else if(average > 80){
          predikat = 'graduate';
      } else if(average > 70){
          predikat = 'participant';
      }
        console.log(i+1 +'. Email: '+arr[i][0]);
        console.log('Rata-rata: ' + average.toFixed(1));
        console.log('Predikat: ' + predikat +'\n');
    }
}

recapScores(data)
  
  