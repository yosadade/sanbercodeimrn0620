var introduce = function (name, age, address, hobby) {
    return `Nama saya ${name}, Umur saya ${age} tahun, Alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}`
}

console.log(introduce('Agus', 30, 'Jln. Malioboro, Yogyakarta', 'Gaming'))